Feature: 
  Assignment 03

  Scenario Outline: Verify first product
    Given I have open app titki
    When Click menu bar
    And Click Danh Muc San Pham
    And Click Điện Gia Dụng
    And Click to Sắp xếp and select Ban chay
    And Click first the product and verify the product name, product price would be displayed.
    And Scroll to <scrollto>  
    Then Verify product category display

    Examples: 
      | scrollto  |
      | Thông tin chi tiết |
