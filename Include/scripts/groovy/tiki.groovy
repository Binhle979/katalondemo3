import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By
import org.openqa.selenium.Keys

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class tiki {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */

	@Given("I have open app titki")
	def openApp() {
		Mobile.startExistingApplication('vn.tiki.app.tikiandroid')
	}

	@When("Click menu bar")
	def iClickMenuBar() {
		//		Mobile.tap(findTestObject('Object Repository/Mobile/tiki/HomePage/search'),30)
		//		Mobile.sendKeys(findTestObject('Object Repository/Mobile/tiki/HomePage/search'), 'abc')
	}

	@And("Click Danh Muc San Pham")
	def iClickDanhMucSanPham() {
		Mobile.tap(findTestObject('Object Repository/Mobile/tiki/HomePage/btn_DanhMuc_mobile'),30)
	}

	@And("Click Điện Gia Dụng")
	def iClickDienGiaDung() {
		CustomKeywords.'MobileKeyword.scrollElement'('//android.widget.TextView[@text="Túi thời trang nam"]')
		
		Mobile.tap(findTestObject('Object Repository/Mobile/tiki/HomePage/av'),30)
		///Mobile.scrollToText(null, FailureHandling.STOP_ON_FAILURE)


	}

	@And("Click to Sắp xếp and select Ban chay")
	def iClickToSapXepAndSelectBanChay() {

		Mobile.tap(findTestObject('Object Repository/Mobile/tiki/HomePage/btn_banchay_mobile'),30)
	}
	@And("Click first the product and verify the product name, product price would be displayed.")
	def iclickFirstProductAndVerifyIt() {
		Mobile.tap(findTestObject('Object Repository/Mobile/tiki/HomePage/first_product_mobile'),30)
	}
	@And("Scroll to (.*)")
	def scrollToThongTinChiTiet(String text){
		CustomKeywords.'MobileKeyword.scrollElement'('//android.widget.TextView[@text="Túi thời trang nam"]')
	}
	@Then("Verify product category display")
	def VerifyProductCategory() {
	}
}