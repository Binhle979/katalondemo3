<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_right click me</name>
   <tag></tag>
   <elementGuidId>950093c2-95c8-4904-9eb6-ce3fd48db5ee</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>span.context-menu-one.btn.btn-neutral</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//body[@id='authentication']/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>d1545fea-164c-44ce-a805-3df168a2ca96</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>context-menu-one btn btn-neutral</value>
      <webElementGuid>fdc769a5-b97f-4212-8df0-104207b82834</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>right click me</value>
      <webElementGuid>1b7b3e43-966c-4a3d-85d3-f82cc84bc589</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;authentication&quot;)/span[@class=&quot;context-menu-one btn btn-neutral&quot;]</value>
      <webElementGuid>448b1806-e5a8-42ef-b822-71e677e42a00</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//body[@id='authentication']/span</value>
      <webElementGuid>63512ef7-cdac-41e1-8b01-49c055ff3da1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='New Tours'])[1]/following::span[1]</value>
      <webElementGuid>44232c14-8ab1-49ca-b73d-112f97280356</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Payment Gateway Project'])[1]/following::span[1]</value>
      <webElementGuid>cbf3ba3a-0053-43bd-adb9-c843556e4668</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Double-Click Me To See Alert'])[1]/preceding::span[1]</value>
      <webElementGuid>9ef74995-0d4b-470f-a4cc-38de4f3f375c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Edit'])[1]/preceding::span[1]</value>
      <webElementGuid>df68db48-5530-4ea8-89c4-84cf2b221172</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='right click me']/parent::*</value>
      <webElementGuid>7e9f1b48-4c2e-4827-be17-8ec2e7d85890</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span</value>
      <webElementGuid>2bca8d4b-f431-419f-899d-ee153ca4ea98</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'right click me' or . = 'right click me')]</value>
      <webElementGuid>d8254336-0865-40b1-bba4-831a160281a7</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
