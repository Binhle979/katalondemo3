<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Click on this check box_isAgeSelected</name>
   <tag></tag>
   <elementGuidId>0cb80568-7f3f-46e1-acad-40f0c4f782ae</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#isAgeSelected</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='isAgeSelected']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>e5ca83e1-8868-4a68-87e3-3635eeccfd59</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>checkbox</value>
      <webElementGuid>c587e18d-d913-41ec-afee-0554a64f3312</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>isAgeSelected</value>
      <webElementGuid>16557a65-00c2-43ad-81eb-d058594ca7f0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;isAgeSelected&quot;)</value>
      <webElementGuid>fda3849f-8524-4d8b-933a-ed942f182751</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='isAgeSelected']</value>
      <webElementGuid>5551bd63-f018-4f2e-b6b6-8cb9f9b26f41</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='easycont']/div/div[2]/div/div[2]/div/label/input</value>
      <webElementGuid>b2655dbd-1bc2-4921-9897-51c38d4100f8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//input</value>
      <webElementGuid>ae7515ac-a4ee-4b18-9e4a-7b10a41bfbdd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'checkbox' and @id = 'isAgeSelected']</value>
      <webElementGuid>07985aff-3c6d-46a6-bba4-ac67f8fe8fc8</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
